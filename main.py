from turtle import *
import Renderer
import time

import random

debug = False

#TODO: add support for more branches

def smaller(num1, num2):
    if (num1 < num2):
        return num1
    else:
        return num2

turtle = Turtle()
turtle.pensize(2)
turtle.speed(10)
turtle.penup()
turtle.setposition(0, 150)

nums = []

for i in range(8):
    nums.append(random.randint(1, 10))

if (debug):
    print("Numbers: " + str(nums))

baseLength = 300
Renderer.drawTree(turtle, baseLength, nums)

turtle.speed(4)

pattern = ""
botNums = nums[len(nums):]
left = 0
right = 0

turtle.pensize(4)

for i in range(2):
    direction = input("please input a direction (left/right): ")

    if (direction == "left"):
        turtle.setheading(225)
        pattern += "0"
        botNums = nums[:int(-8 / 2)]
    else:
        turtle.setheading(225 + 90)
        pattern += "1"
        botNums = nums[int(8 / 2):]

    turtle.pencolor("purple")
    turtle.forward(int(baseLength / pow(2, i)))

    i += 1

    if not (i < 2):
        break;

    print("My turn...")
    time.sleep(2)

    left = smaller(botNums[0], botNums[1])
    right = smaller(botNums[2], botNums[3])

    if (left > right):
        turtle.setheading(225)
        pattern += "0"
    else:
        turtle.setheading(225 + 90)
        pattern += "1"
    turtle.forward(int(baseLength / (2 ** i)))
    i += 1
    baseLength /= 2

if (debug):
    print("pattern: " + pattern)

i = nums[int(int(pattern, 2))]

print("Number: " + str(i))

turtle.penup()
turtle.hideturtle()
turtle.goto(0, 300)

if (i >= 5):
    turtle.pencolor("red")
    print("I win!! :)")
    Renderer.drawText("I win!! :)", 30, turtle)
else:
    turtle.pencolor("green")
    print("You win!! :(")
    Renderer.drawText("You win!! :(", 30, turtle)

input()