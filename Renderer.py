from turtle import *
import math

def drawText(text: str, fontSize: int, turtle: Turtle):
    turtle.write(text, align="center", font=("Arial", fontSize, ("bold", "italic")))

def drawTree(turtle: Turtle, baseLength, nums):
    turtle.pendown()
    tmpLength = baseLength
    pattern = "000"
    for k in range(8):
        for j in range(len(pattern)):
            if (pattern[j] == "0"):
                turtle.setheading(225)
            else:
                turtle.setheading(225 + 90)

            turtle.forward(tmpLength)
            tmpLength = baseLength / pow(2, j + 1)

        turtle.penup()
        turtle.setheading(270)
        turtle.forward(30)
        drawText(str(nums[k]), 14, turtle)

        turtle.setposition(0, 150)
        turtle.pendown()
        tmpLength = baseLength
        pattern = str(bin(k + 1))[2:]
        if (len(pattern) < 3):
            for i in range(3 - len(pattern)):
                pattern = "0" + pattern